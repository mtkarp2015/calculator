import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// Global Variables
var globalHistoryData = null;
var userId = '';
const backendUrl = "https://mtkarp-calc-demo.herokuapp.com";
const connectSuffix = "/api/connect";
const newEquationSuffix = "/api/new";
const getEquationSuffix = "/api/all";
const historyUpdatePeriod = 1000;
const maxDisplayLength = 15;
const truncationLength = 10;


/* postEquationToServer sends the equation entered by the user to the server 
   so that all other users can see the equation which was executed.
   
   equation - a String of the equation entered
   user - a String of the user's userId */
function postEquationToServer(equation, user) {
  let data = {"Content": equation, "User": user};
  var request = new XMLHttpRequest();
  request.open('POST', backendUrl + newEquationSuffix);
  request.send(JSON.stringify(data));
}

/* CalcButton returns the markup code for displaying a calculator button

   props - an integer which corresponds to the underlying value which will 
           be displayed on the button. */
function CalcButton(props) {
  return (
    <button className="calcbutton" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

/* DelClearButton returns the markup code for displaying a the 
   delete and clear buttons

   props - an integer which corresponds to the underlying value which will 
           be displayed on the button. */
function DelClearButton(props) {
  return (
    <button className="delbutton" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

/* updateUsername returns a string of the user's identification number
   if the identification number is not the empty string, otherwise it
   returns the empty string so that no partial data is given to the UI. */
function updateUsername() {
  let tempString = '';
  if (userId !== '') {
    tempString = "You are: " + userId;
  }
  return tempString;
}

/* attemptUpdate passes JSON data received from the backend into a global 
   variable for later processing and display.

   serverHistory - the JSON data containing the last 10 equations entered
                   by users.
   globalHistoryData - a global variable which holds the latest server 
                       history data after the function is executed. */
function attemptUpdate(serverHistory) {
  globalHistoryData = serverHistory;
}

/* The CalculatorLayout class handles the layout for all of the calculator 
   buttons. */
class CalculatorLayout extends React.Component {

  /* renderCalcButton returns the markup code for a calculator button.

     i - an integer which corresponds to the value to be displayed on the 
         button.
  */
  renderCalcButton(i) {
    return (
      <CalcButton
        value = {this.props.layoutText[i]}
        onClick = {() => this.props.onClick(i)}
      />
    );
  }

  /* renderDelClearButton returns the markup code for the delete and clear 
     buttons.

     i - an integer which corresponds to the value to be displayed on the 
         button.
  */
  renderDelClearButton(i) {
    return (
      <DelClearButton
        value = {this.props.layoutText[i]}
        onClick = {() => this.props.onClick(i)}
      />
    );
  }

  /* render provides the markup for the entire calculator layout. */
  render() {  
    return (
      <div>
        <div className="calc-row">
          {this.renderDelClearButton(16)}
          {this.renderDelClearButton(17)}
        </div>
        <div className="calc-row">
          {this.renderCalcButton(0)}
          {this.renderCalcButton(1)}
          {this.renderCalcButton(2)}
          {this.renderCalcButton(3)}
        </div>
        <div className="calc-row">
          {this.renderCalcButton(4)}
          {this.renderCalcButton(5)}
          {this.renderCalcButton(6)}
          {this.renderCalcButton(7)}
        </div>
        <div className="calc-row">
          {this.renderCalcButton(8)}
          {this.renderCalcButton(9)}
          {this.renderCalcButton(10)}
          {this.renderCalcButton(11)}
        </div>
        <div className="calc-row">
          {this.renderCalcButton(12)}
          {this.renderCalcButton(13)}
          {this.renderCalcButton(14)}
          {this.renderCalcButton(15)}
        </div>
      </div>
    );
  }
}

/* The HistoryLayout class handles the layout for the calculator 
   history data from the backend server. */
class HistoryLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      historyData: Array(10).fill(['', '']),
    };
  }

  /* componentDidMount is called once the HistoryLayout is mounted. 
     This in turn starts a timer to update the history data after 
     historyUpdatePeriod milliseconds. */
  componentDidMount() {
    this.interval = setInterval(() => this.tick(), historyUpdatePeriod);
  }
  
  /* componentWillUnmount clears the interval to prevent a memory leak 
     in the application */
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  /* tick is called periodically to gather history data from the 
     backend server. tick will then invoke the proper functions to 
     update the HistoryLayout's history data. */
  tick() {
    // Update history data from server
    var request = new XMLHttpRequest();
    request.open('GET', backendUrl + getEquationSuffix);
    request.responseType = 'json';
    request.send();
    // wait for server response and update
    request.onload = function () {
      var serverHistory = request.response;
      attemptUpdate(serverHistory); 
    }
    this.updateHistory();
  }

  /* updateHistory handles the updates of the history data contained 
     in the HistoryLayout */
  updateHistory() {
    var updatedHistory = this.state.historyData;
    if (globalHistoryData != null) {
      for (let i = 0; i < globalHistoryData.length; i++) {
        updatedHistory[i] = [globalHistoryData[i]['Content'], globalHistoryData[i]['User']];
      }
    }
    this.setState({
      historyData: updatedHistory,
    });
  }

  /* render provides the markup for the entire history layout. */
  render() {
    return (
      <div className="history-display">
        <div className="history-title">
          History Data:
        </div>
        <div className="history-data">
          <div className="historyelement">
            {this.state.historyData[0][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[1][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[2][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[3][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[4][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[5][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[6][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[7][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[8][0]}
          </div>
          <div className="historyelement">
            {this.state.historyData[9][0]}
          </div>
        </div>
        <div className="history-user">
          <div className="user">
            {this.state.historyData[0][1]}
          </div>
          <div className="user">
            {this.state.historyData[1][1]}
          </div>
          <div className="user">
            {this.state.historyData[2][1]}
          </div>
          <div className="user">
            {this.state.historyData[3][1]}
          </div>
          <div className="user">
            {this.state.historyData[4][1]}
          </div>
          <div className="user">
            {this.state.historyData[5][1]}
          </div>
          <div className="user">
            {this.state.historyData[6][1]}
          </div>
          <div className="user">
            {this.state.historyData[7][1]}
          </div>
          <div className="user">
            {this.state.historyData[8][1]}
          </div>
          <div className="user">
            {this.state.historyData[9][1]}
          </div>
        </div>
      </div>
    );
  }
}

/* Calculator holds the client-side data for the calculator functionality 
   and handle all button presses from the user. Calculator also holds all 
   of the underlying display, calculator layout, and history layout user 
   interface elements. */
class Calculator extends React.Component {
  /* Setup the calculator button values, displays, and other logical 
     variables */
  constructor(props) {
    const calcKeys = ['7', '8', '9', '/', '4', '5', '6', '*', '1', '2', '3', '-', '0', '.', '=', '+', 'CLEAR', 'DEL'];
    super(props);
    this.state = {
      calcButtonText: calcKeys,
      displayText: '',
      decimalExists: false,
      currentCharacterIsOperator: false,
      count: 0,
      tempX: 0,
    };
    this.connect();
  }

  /* connect initializes a connection with the backend server to negotiate
     a user identifier (just an integer at this time). 
     
     Note that this does not provide any user identification or an ability to 
     select a user name at this time. This can be added in the future if
     desired. 
     
     userId - String which will contain the user identification information
              upon successful completion. */
  connect() {
    // handle connection to back end
    var request = new XMLHttpRequest();
    request.open('GET', backendUrl + connectSuffix);
    request.responseType = 'json';
    request.send();
    // wait for server response and update the userId global variable
    request.onload = function () {
      var id = request.response;
      userId = 'User ' + String(id['UserId']);
    }
  }

  /* handleClear clears the display and updates underlying data 
     for consistency. */
  handleClear() {
    this.setState({
      displayText: '',
      decimalExists: false,
      currentCharacterIsOperator: false,
      count: 0,
    });
  }

  /* handleDelete deletes the last character in the display string and 
     updates underlying data for consistency. */
  handleDelete() {
    // check if we're at an operator
    if (this.state.currentCharacterIsOperator) {
      // We're deleting an operator, reset the variable tracking this.
      this.setState({
        currentCharacterIsOperator: false,
      });
      // determine if the previous number has a '.' character:
      let j = this.state.displayText.length-1;
      let done = false;
      while (!done) {
        if (j>0) {
          let c = this.state.displayText.substr(j-1, 1);
          // found a decimal in the number
          if (c === '.') {
            this.setState({
              decimalExists: true,
            });
            done = true;
            break;
          }
          // reached the end of the number
          if (c === '/' || c === '*' || c === '-' || c === '+') {
            this.setState({
              decimalExists: false,
            });
            done = true;
            break;
          }
        } else { // reached the beginning of the string
          this.setState({
            decimalExists: false,
          });
          done = true;
          break;
        }
        j--;
      }
    } else {
      // do nothing
    }
    // Check if we're deleting the '.' character
    if (this.state.displayText.substr(this.state.displayText.length-2, 1) === '.') {
      this.setState({
        decimalExists: false,
      });
    } else {
      // do nothing
    }
    // delete the character
    if (this.state.count > 0) {
      this.setState({
        count: this.state.count-1,
        displayText: this.state.displayText.substr(0,this.state.displayText.length-1),
      });
    } else {
      // do nothing
    }
    // see if we are now dealing with an operator:
    let tempChar = this.state.displayText.substr(this.state.displayText.length-2, 1);
    if ((tempChar === '/') ||
        (tempChar === '*') ||
        (tempChar === '-') ||
        (tempChar === '+'))
    {
      this.setState({
        currentCharacterIsOperator: true,
      });
    } else {
      // do nothing
    }
  }

  /* handleEquals parses the display text and calculates the result.
     The function also pushes data to the backend server so that all
     clients are updated with the equation data. */
  handleEquals() {
    // utilize eval to compute the result
    let evaluationString = '';
    let c = this.state.displayText.substr(this.state.displayText.length-1, 1);
    if (c === '/' || c === '*' || (c === '-') || c === '+') {
      // last character in the display text is an operator, ignore.
      evaluationString = this.state.displayText.substr(0, this.state.displayText.length-1);
    } else {
      evaluationString = this.state.displayText;
    }
    // eslint-disable-next-line
    let result = eval(evaluationString);

    //let tempString = String(sum).substr(0,maxDisplayLength), decimalFound = null;
    let tempString = String(result), decimalFound = null;
    if (tempString.indexOf('.') !== -1) {
      decimalFound = true;
    } else {
      decimalFound = false;
    }

    // Post the data to the server
    postEquationToServer(this.state.displayText + '=' + tempString, userId);

    // update the display with the result and update underlying data 
    // for consistency.
    this.setState({
      decimalExists: decimalFound,
      currentCharacterIsOperator: false,
      displayText: tempString.substr(0,truncationLength),
      count: tempString.substr(0,truncationLength).length,
    });
  }

  /* handleMultiplicationDivisionAddition adds the specified operator to the
     display text if it is valid. If an operator is already at the end of the
     display text, the operator is replaced with the new operator.
     
     i - integer which corresponds to the underlying text of the button which
         was pressed. */
  handleMultiplicationDivisionAddition(i) {
    if (this.state.count > 0 && this.state.count < maxDisplayLength) {
      if (!this.state.currentCharacterIsOperator) {
        this.setState({
          currentCharacterIsOperator: true,
          count: this.state.count+1,
          decimalExists: false,
          displayText: this.state.displayText + this.state.calcButtonText[i],
        });
      } else { // current character is an operator
        // if the current character is '-' AND the previous character is an operator or null, do not add it.
        if ( (this.state.displayText.substr(this.state.displayText.length - 1, 1) === '-') && 
             ((this.state.displayText.substr(this.state.displayText.length - 2, 1) === '+') || 
              (this.state.displayText.substr(this.state.displayText.length - 2, 1) === '/') || 
              (this.state.displayText.substr(this.state.displayText.length - 2, 1) === '*') ||
              ((this.state.displayText.length - 2) < 0)) ) {
          // take no action
        } else {
          this.setState({
            currentCharacterIsOperator: true,
            displayText: this.state.displayText.substr(0,this.state.displayText.length-1) + this.state.calcButtonText[i],
          });
        }
      }
    }
  }

  /* handleSubtraction adds the subtraction operator to the
     display text if it is valid. If the addition operator is already at the 
     end of the display text, the operator is replaced with the subtraction
     operator.
     
     i - integer which corresponds to the underlying text of the button which
         was pressed. */
  handleSubtraction(i) {
    if (this.state.count < maxDisplayLength) {
      if (!this.state.currentCharacterIsOperator) {
        this.setState({
          currentCharacterIsOperator: true,
          count: this.state.count+1,
          decimalExists: false,
          displayText: this.state.displayText + this.state.calcButtonText[i],
        });
      } else {
        // get the previous operator
        let c = this.state.displayText.substr(this.state.displayText.length-1, 1);
        if (c === '/' || c === '*') {
          // negative number can be added after these operations
          this.setState({
            currentCharacterIsOperator: true,
            count: this.state.count+1,
            decimalExists: false,
            displayText: this.state.displayText + this.state.calcButtonText[i],
          });
        } else { // + operator, replace
          this.setState({
            currentCharacterIsOperator: true,
            displayText: this.state.displayText.substr(0,this.state.displayText.length-1) + this.state.calcButtonText[i],
          });
        }
      }
    }
  } 

  /* handleDecimal adds a decimal point to the number if the current number
     does not already contain a decimal point. If the number does contain
     a decimal point, no decimal point is added. Note that decimal points
     can be used to start a number. 
     
     i - integer which corresponds to the underlying text of the button which
         was pressed. */
  handleDecimal(i) {
    if (this.state.count < maxDisplayLength) {
      if (!this.state.decimalExists) {
        this.setState({
          decimalExists: true,
          currentCharacterIsOperator: false,
          count: this.state.count+1,
          displayText: this.state.displayText + this.state.calcButtonText[i],
        });
      } else {
        // Cannot add decimal a second time.
      }
    }
  }

  /* handleOperands adds the number to the current number / display text
     so long as the length of the number is less than the limit of the
     display text length.
     
     i - integer which corresponds to the underlying text of the button which
         was pressed. */
  handleOperands(i) {
    if (this.state.count < maxDisplayLength) {
      this.setState({
        currentCharacterIsOperator: false,
        count: this.state.count+1,
        displayText: this.state.displayText + this.state.calcButtonText[i],
      });
    }
  }

  /* handleClick is the main processing function for all user button clicks.
     This includes passing data to the backend server and updating the
     display with equation information. */
  handleClick(i) {
    switch(this.state.calcButtonText[i]) {
      case 'CLEAR':
        this.handleClear();
        break;
      case 'DEL':
        this.handleDelete();
        break;
      case '=':
        this.handleEquals();
        break;
      case '/':
      case '*':
      case '+':
        this.handleMultiplicationDivisionAddition(i);
        break;
      case '-':
        this.handleSubtraction(i);
        break;
      case '.':
        this.handleDecimal(i);
        break;
      default: // all numeric values
        this.handleOperands(i);
    }
  }

  /* render provides the markup for the calculator, including displays, 
     the calculator layout, and history layout. */
  render() {
    return (
      <div className="calculator">
        <div className="calculator-display">
          <div>
            <div className="calc-row">
              <div className="display">{this.state.displayText}</div>
            </div>
            <CalculatorLayout
              layoutText = {this.state.calcButtonText}
              onClick = {(i) => this.handleClick(i)}
            />
            <div className="calc-row">
              <div className="display">{updateUsername()}</div>
            </div>
          </div>  
          <div>
            <HistoryLayout/>
          </div>
        </div>
      </div>
    );
  }

}

ReactDOM.render(
  <Calculator />,
    document.getElementById('root')
);
